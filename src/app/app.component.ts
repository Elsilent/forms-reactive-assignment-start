import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  projectForm: FormGroup;
  statuses: ['Stable', 'Critical', 'Finished'];

  ngOnInit() {
    this.projectForm = new FormGroup({
      'projectName': new FormControl(
        null,
        [Validators.required],
        this.validateProjectNameAsync.bind(this)
      ),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'projectStatus': new FormControl(null)
    });
  }

  onSubmit() {
    console.log(this.projectForm.value);
  }

  validateProjectName(control: FormControl): {[s: string]: boolean} {
    if (control.value === 'Test') {
      return {'projectNameInvalid': true};
    }

    return null;
  }

  validateProjectNameAsync(control: FormControl): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        resolve(this.validateProjectName(control));
      }, 1000);
    });
  }
}
